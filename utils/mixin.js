let rewardedVideoAd = null;
let interstitialAd = null;
const mixin = {
    data() {
        return {
            isInters: false, //在需要的页面开启
            interstitiaAdOption: {},
            videoAdOption: {},
            interstitialAd: null,
            dataList: [{
                name: '来吧，在民国结婚',
                path: '/pages/index/detail_jyz',
                id: 0,
                img: "http://www.tejiawang.work/imgs/jiehz/01.png",
                thumb: "http://www.tejiawang.work/imgs/jiehz/01.jpg"
            }, {
                name: '最佳男友送的礼物',
                id: 3,
                path: '/pages/index/detail_bsj',
                thumb: 'http://www.tejiawang.work/imgs/thumb/bsj.jpg',
                img: "http://www.tejiawang.work/imgs/origin/bsj.jpg"
            },
                {
                    name: '我被清华大学录取啦',
                    id: 1,
                    path: '/pages/index/detail_qhdx',
                    thumb: "http://www.tejiawang.work/imgs/thumb/qinghua1.jpg",
                    img: "http://www.tejiawang.work/imgs/origin/qinghua1.jpg"
                },
                {
                    name: '火爆-山河大学录取通知书',
                    id: 6,
                    path: '/pages/index/detail_shdx',
                    thumb: "http://www.tejiawang.work/imgs/thumb/shdx.png",
                    img: "http://www.tejiawang.work/imgs/origin/shanhedaxue.jpg"
                },
                {
                    name: '看看我微信零钱余额',
                    id: 2,
                    path: '/pages/index/detail_wxlq',
                    thumb: 'http://www.tejiawang.work/imgs/thumb/lingqian.jpg',
                    img: 'http://www.tejiawang.work/imgs/origin/weixinlingqian.png',
                },
                {
                    name: '令氏专用金条',
                    id: 4,
                    path: '/pages/index/detail_lsjt',
                    thumb: 'http://www.tejiawang.work/imgs/thumb/jintiao.jpg',
                    img: "http://www.tejiawang.work/imgs/origin/jintiao.png"
                }, {
                    name: '高启强来电话啦',
                    id: 5,
                    path: '/pages/index/detail_gqq',
                    thumb: '/static/gqq.jpg',
                    img: '/static/gqq.jpg'
                },
            ],
        };
    },
    onLoad(opts) {
        if (opts.dId) {
            this.detail = this.dataList.filter(val => val.id == opts.dId)[0]
        }
    },
    methods: {
        //合成图片，延时5秒跳转，用于展示插屏广告。分享直接用base64分享
        getImg(e) {
            const vm = this

            setTimeout(() => {
                vm.$util.hideLoading()
                vm.$util.replaceUrl("/pages/result/result?src=" + e + "&name=" + this.detail.name)
            }, 5000)
        },
        navTo(url) {
            uni.navigateTo({
                url,
            });
        },
        back(num) {
            if (typeof num === 'object' || !num) {
                num = 1;
            }
            uni.navigateBack({
                delta: num,
            });
        },

        // 插屏广告仅app、微信小程序、QQ小程序支持。字节写的不支持，但实际创建是可以的，只是调用方式有差异。
        createInterstitialAd() {
            let that = this;
            if (this.interstitialAd) {
                return
            }
            this.interstitialAd = interstitialAd = uni.createInterstitialAd(this.interstitiaAdOption);
            interstitialAd.onLoad(() => {
                console.log('插屏 广告加载成功');
            });
            interstitialAd.onClose(() => {
                console.log('插屏 广告关闭');

                // #ifdef MP-TOUTIAO
                interstitialAd.destroy();
                that.createInterstitialAd();
                // #endif
            });
            interstitialAd.onError((err) => {
                console.log('插屏 广告加载失败');
            });
        },
        // 激励视频广告支持app、微信小程序、字节跳动小程序、qq小程序、快手小程序
        createVideoAd() {
            /*	if (rewardedVideoAd){
                    return
                }*/
            rewardedVideoAd = uni.createRewardedVideoAd(this.videoAdOption);
            rewardedVideoAd.onLoad(() => {
                this.isVideoAdLoading = true;
                uni.hideLoading();
                console.log('激励 视频加载成功');
            });
            rewardedVideoAd.onError((err) => {
                this.isVideoAdLoading = false;
                uni.hideLoading();
                console.log('激励 视频加载失败', err);
            });
            rewardedVideoAd.onClose((res) => {
                uni.hideLoading();
                console.log('激励 视频关闭');
                this.endVideoAd(res);
            });
        },
        showVideoAd() {
            if (!rewardedVideoAd) {
                this.$util.showToast('广告加载失败,请重试~');
                return false;
            }

            rewardedVideoAd.show().catch(() => {
                console.log('重新加载广告');
                rewardedVideoAd
                    .load()
                    .then(() => rewardedVideoAd.show())
                    .catch((err) => {
                        this.isVideoAdLoading = false;
                        console.log('激励视频 广告显示失败');
                        this.$util.showToast('广告上限拉-请明天再试~');
                    });
            });
        },
        showIntersAD() {
            if (interstitialAd) {
                interstitialAd
                    .show()
                    .then(() => {
                        // #ifdef MP-TOUTIAO
                        interstitialAd.destroy();
                        createInterstitialAd();
                        // #endif
                    })
                    .catch((err) => {
                        console.error('插屏广告加载失败', err);
                    });
            }
        },
        endVideoAd(res) {
            if (res && res.isEnded) {
                console.log('ad end');
                this.addUseCount();
            } else {
                this.$util.showToast('广告未看完~');
            }
        },
    },
    onReady() {
        if (this.detail && this.detail.name) {
            uni.setNavigationBarTitle({
                title: this.detail.name
            })
        }
        const pages = getCurrentPages()
        if (pages[pages.length - 1] && pages[pages.length - 1].route === "pages/index/index") {
            return;
        }
        let adInfo = this.$util.getStorageByPhone('adInfo');
        if (!adInfo) {
            return;
        }
        // #ifdef MP-WEIXIN

        if (adInfo.wx_interstitial_ad_id) {
            this.interstitiaAdOption = {
                adUnitId: adInfo.wx_interstitial_ad_id,
            };
            // 创建插屏广告实例
            this.createInterstitialAd();
        }

        if (adInfo.wx_video_ad_id) {
            this.videoAdOption = {
                adUnitId: adInfo.wx_video_ad_id,
            };
            // 创建激励视频广告
            this.createVideoAd();
        }
        // #endif

        // #ifdef MP-TOUTIAO
        if (adInfo.tt_interstitial_ad_id) {
            this.interstitiaAdOption = {
                adUnitId: adInfo.tt_interstitial_ad_id,
            };
            // 创建插屏广告实例，不用根据文档调用插屏广告。插屏间隔60s,基本上不会有打开第二次的机会，也就不处理了。
            this.createInterstitialAd();
        }
        if (adInfo.tt_video_ad_id) {
            this.videoAdOption = {
                adUnitId: adInfo.tt_video_ad_id,
            };
            // 创建激励视频广告
            this.createVideoAd();
        }
        // #endif
    },
    onShareTimeline() {

    },
    onShareAppMessage() {

    }
};

export default mixin;
