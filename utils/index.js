import moment from "moment";

export default {
    //显示loading加载框
    showLoading(title) {
        uni.showLoading({
            title: title || "加载中...",
            mask: true,
        })
    },
    //隐藏loading加载框
    hideLoading() {
        uni.hideLoading()
    },
    showConfirm(title, content, callback, confirmText, cancelText) {
        uni.showModal({
            title: title || "提示",
            content: content,
            confirmText: confirmText || "确定",
            cancelText: cancelText || "取消",
            success: (res) => {
                callback(res)
            },
        })
    },
    showToast(title) {
        uni.showToast({
            title: title,
            icon: "none"
        })
    },

    showAlert(title, content, callback, confirmText) {
        uni.showModal({
            title: title || "提示",
            content: content,
            confirmText: confirmText || "确定",
            showCancel: false,
            success: (res) => {
                callback(res)
            },
        })
    },
    showActionSheet(itemList, callback) {
        uni.showActionSheet({
            itemList: itemList,
            success: (res) => {
                callback(res.tapIndex)
            },
        })
    },
    momentYearMonth(str, type) {
        const time = str ? str : new Date().getTime()
        return moment(time).format(type ? type : "YYYY-MM")
    },
    //拨打电话
    callTel(tel) {
        uni.makePhoneCall({
            phoneNumber: tel.toString(),
        })
    },
    //跳转页面push
    pushUrl(url, animationType, time) {
        uni.navigateTo({
            url: url,
            animationType: animationType || "pop-in",
            animationDuration: time || 300,
        })
    },
    //跳转页面replace
    replaceUrl(url) {
        uni.redirectTo({
            url: url,
        })
    },
    //关闭所有页面之后跳转页面
    reLaunchUrl(url) {
        uni.reLaunch({
            url: url,
        })
    },
    //跳转到tabBar页面
    switchTabUrl(url) {
        uni.switchTab({
            url: url,
        })
    },
    //关闭当前页面，返回上一页面或多级页面
    back(delta, animationType, time) {
        uni.navigateBack({
            delta: delta || 1,
            animationType: animationType || "pop-out",
            animationDuration: time || 300,
        })
    },
    naviSuccess(msg, path, type) {
        this.showToast(msg || "成功")
        var tiemout = setTimeout(() => {
            clearTimeout(tiemout)
            if (path) {
                if (type === "replace") {
                    this.replaceUrl(path)
                } else if (type === "switch") {
                    this.switchTabUrl(path)
                } else {
                    this.pushUrl(path)
                }
            } else {
                this.back()
            }
        }, 1500)
    },
    //手机号正则验证
    checkPhone(phone) {
        if (/^(1[3456789][0-9])\d{8}$/.test(phone)) {
            return true
        } else {
            return false
        }
    },
    //身份证号正则验证
    checkCard(card) {
        let reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/
        if (reg.test(card)) {
            return true
        } else {
            return false
        }
    },
    // 存储
    setStorageByPhone(key, value) {
        uni.setStorageSync(`${key}`, value)
    },
    getStorageByPhone(key) {
        return uni.getStorageSync(`${key}`) || ""
    },
    //获取设备信息
    getDeviceInfo() {
        let systemInfo = uni.getSystemInfoSync()
        return `${systemInfo.brand} ${systemInfo.model} ${systemInfo.platform} ${systemInfo.system}`
    },
    //判断是安卓还是苹果手机
    getDeviceType() {
        let platform = uni.getSystemInfoSync().platform
        return platform
    },
    dateFormat(time, fomatType) {
        let date = new Date(time)
        let year = date.getFullYear()
        // 在日期格式中，月份是从0开始的，因此要加0，使用三元表达式在小于10的前面加0，以达到格式统一  如 09:11:05
        let month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1
        let day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate()
        let hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours()
        let minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()
        let seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds()
        // 拼接
        // return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
        console.log("hours ====  ", hours)
        console.log("minutes ====  ", minutes)
        if (fomatType === "hh:mm") {
            return hours + ":" + minutes
        }
        return year + "-" + month + "-" + day
    },
    checkLogin(app) {
        if (!app.userInfo && !app.userInfo.id) {
            this.pushUrl("/pages/login/login")
        }
    },
}
