import Vue from "vue"
import App from "./App"
import Utils from "./utils"
import uView from "@/uni_modules/uview-ui"
Vue.use(uView)
Vue.config.productionTip = false
App.mpType = "app"
import mixin from "./utils/mixin.js"
import VideoAD from "./components/videoAD";
Vue.component("video-a-d",VideoAD)
Vue.mixin(mixin)

Vue.prototype.$util = Utils
const app = new Vue({
  ...App,
})
app.$mount()
